#ifndef __OS_PORT_H__
#define __OS_PORT_H__

#define OSP_MAX_SYN_QUEUE 10
#define OSP_MAX_FULL_PATH 1024

#define OSP_TRUE  1
#define OSP_FALSE 0

/* DWORD (thread id) is used for sys_thread_t but we won't include windows.h */
typedef void* osp_thread_t;

/** Function prototype for thread functions */
typedef uint32 (*osp_f_thread_t)(void * pctxt);

void osp_timer_f_sleep(uint32 unMilliSeconds);

/*
 * initialize a socket
 *
 * @param: none
 *
 * return: sockfd, a socket description
 */
sint32 osp_socket_f_open(int af, int type, int protocol);

/*
 * bind socket with address-port
 *
 * @param: none
 *
 * return: sockfd, a socket description
 */
sint32 osp_socket_f_bind(sint32 sockfd, uint16 uport);

/*
 * listen on a port
 *
 * @param: sockfd, a socket description
 * @param: ipaddr, the IP of remote host
 * @param: uport, the port of remote host 
 *
 * return: 1, success; 0, fail
 */
sint32 osp_socket_f_connect(sint32 sockfd, char * ipaddr, uint16 uport);

/*
 * listen on a port
 *
 * @param: sockfd, a socket description
 * @param: backlog, length of waiting queue
 *
 * return: 1, success; 0, fail
 */
sint32 osp_socket_f_listen(sint32 sockfd, uint16 backlog);

/*
 * waiting and receiving connect-request from remote host
 *
 * @param: lis_sockfd, a socket description
 *
 * return: apt_sockfd, a socket description
 */
sint32 osp_socket_f_accept(sint32 lis_sockfd);

/*
 * receive data by socket
 *
 * @param: sockfd, a socket description
 * @param: up_buff, a buffer for saving data 
 * @param: buff_size, max length of data that can be received
 *
 * return: the length of data has been received
 */
sint32 osp_socket_f_read(sint32 sockfd, uint08 * up_buff, sint32 buff_size);

/*
 * receive data by socket
 *
 * @param: sockfd, a socket description
 * @param: up_buff, a buffer for saving data 
 * @param: buff_size, max length of data that can be received
 *
 * return: the length of data has been received
 */
sint32 osp_socket_f_readfrom(sint32 sockfd, uint08 * up_buff, sint32 buff_size, 
                             void * addr_from, int * addr_len);

/*
 * send data by socket
 *
 * @param: sockfd, a socket description
 * @param: up_buff, point to the header of data 
 * @param: len, length of data that will be sent
 *
 * return: the length of data has been sent
 */
sint32 osp_socket_f_write(sint32 sockfd, uint08 * up_buff, sint32 len);

/*
 * send data by socket
 *
 * @param: sockfd, a socket description
 * @param: up_buff, point to the header of data 
 * @param: len, length of data that will be sent
 *
 * return: the length of data has been sent
 */
sint32 osp_socket_f_writeto(sint32 sockfd, uint08 * up_buff, sint32 len, 
                            void * addr_to, int addr_len);

/*
 * close connection
 *
 * @param: sockfd, a socket description
 *
 * return: none
 */
void osp_socket_f_close(sint32 sockfd);

uint32 osp_socket_f_getpeeraddr(sint32 sockfd);

sint32 osp_socket_f_geterror(void);

/*
 * open a file and return file description
 *
 * @param: u_rel_directory, relative directory
 * @param: u_file_name, file-name of the specified file
 *
 * return: fp_open, file description.
 */
FILE * osp_file_f_fopen(uint08 * p_rel_directory, uint08 * p_file_name, uint08 * p_open_mode);

sint32 osp_thread_f_open(osp_f_thread_t fp_thread_entry, void * pctxt, void * pthreadid);

sint32 osp_thread_f_close(void *pthread);

#endif /* __OS_PORT_H__ */

