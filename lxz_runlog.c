#include "lxz_config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <time.h>

#include "lxz_types.h"
#include "lxz_runlog.h"

static FILE* gfp_log_file = NULL;

sint32 lxz_log_f_init(uint08* p_root_directory)
{
    int i_ret_val = 0;

    char llbuf[64];
    int i_str_len = 0;
    FILE* fp_pre_file = NULL;
    FILE* fp_log_file = NULL;
    sint32 i_dir_exist = 0;
    sint08 p_real_directory[128];
    
    time_t atime;
    int i_cur_year;
    int i_cur_mon;
    int i_cur_day;
    int i_cur_hour;
    int i_cur_min;
    int i_cur_sec;
    struct tm* p = NULL;

    time(&atime);
    p = gmtime(&atime);

    memset(llbuf, 0, sizeof(llbuf));
    i_cur_year = p->tm_year+1900;
    i_cur_mon = p->tm_mon+1;
    i_cur_day = p->tm_mday;
    i_cur_hour = p->tm_hour+8;
    i_cur_min = p->tm_min;
    i_cur_sec = p->tm_sec;

    memset(p_real_directory, 0, sizeof(p_real_directory));
    strcpy(p_real_directory, LXZ_DEF_LOG_DIR);
    if (p_root_directory != NULL)
    {
        sprintf(p_real_directory, "%s/%s", p_root_directory, LXZ_DEF_LOG_DIR);
    }

    sprintf(llbuf, "./%s/udt%d%02d%02d%02d%02d%02d.log", p_real_directory, i_cur_year, 
        i_cur_mon, i_cur_day, i_cur_hour, i_cur_min, i_cur_sec);

    fp_log_file = fopen(llbuf, "wb+");
    if (fp_log_file == NULL)
    {
        return i_ret_val;
    }

    i_ret_val = 1;
    fp_pre_file = gfp_log_file;
    gfp_log_file = fp_log_file;
    fflush(fp_log_file);

    if (fp_pre_file != NULL)
    {
        memset(llbuf, 0, sizeof(llbuf));
        i_str_len = sprintf(llbuf, "%sudt%d%02d%02d%02d%02d%02d.log", 
            "Next-logfile: ",i_cur_year, i_cur_mon, 
            i_cur_day, i_cur_hour, i_cur_min, i_cur_sec);
        fwrite(llbuf, 1, i_str_len, fp_pre_file);
        fflush(fp_pre_file);
        fclose(fp_pre_file);
    }

    return i_ret_val;
}

sint32 lxz_log_f_printf(const char * vfmt, ...)
{
    char llbuf[512];

    va_list ap;
    int i_str_len = 0;
    FILE* fp_log_file = gfp_log_file;

    time_t atime;
    int i_cur_year;
    int i_cur_mon;
    int i_cur_day;
    int i_cur_hour;
    int i_cur_min;
    int i_cur_sec;
    struct tm* p = NULL;

    if (fp_log_file == NULL)
    {
        return 0;
    }

    time(&atime);
    p = gmtime(&atime);

    i_cur_year = p->tm_year+1900;
    i_cur_mon = p->tm_mon+1;
    i_cur_day = p->tm_mday;
    i_cur_hour = p->tm_hour+8;
    i_cur_min = p->tm_min;
    i_cur_sec = p->tm_sec;

    memset(llbuf, 0, sizeof(llbuf));
    i_str_len = sprintf(llbuf, "%d%02d%02d%02d%02d%02d ", 
        i_cur_year, i_cur_mon, i_cur_day, 
        i_cur_hour, i_cur_min, i_cur_sec);

    va_start(ap,vfmt);
    i_str_len = vsprintf(&(llbuf[i_str_len]), vfmt, ap);
    va_end(ap);

    i_str_len = fwrite(llbuf, 1, strlen(llbuf), fp_log_file);
    fflush(fp_log_file);

    return i_str_len;
}

sint32 lxz_log_f_fsize(void)
{
    sint32 i_file_size = 0;
    FILE* fp_log_file = gfp_log_file;

    if (fp_log_file != NULL)
    {
        i_file_size = ftell(fp_log_file);
    }
    
    return i_file_size;
}

#define LXZ_HEX_OCTET_NUM 16
sint32 lxz_log_f_printfhex(const char * vbuf, sint32 ilen)
{
    sint32 i = 0;
    char * pbuf = (char *)vbuf;
    char lhexbuf[4 * LXZ_HEX_OCTET_NUM];

    lxz_log_f_printf("\r\n", lhexbuf);
    lxz_log_f_printf("================HEX-String-Begin================\r\n");
    while (ilen >= LXZ_HEX_OCTET_NUM)
    {
        i = 0;
        memset(lhexbuf, 0, sizeof(lhexbuf));
        while (i < LXZ_HEX_OCTET_NUM)
        {
            sprintf(&(lhexbuf[3 * i]),"%02x ", pbuf[i]);
            i++;
        }
        lxz_log_f_printf("HEX==%s\r\n", lhexbuf);

        ilen = ilen - LXZ_HEX_OCTET_NUM;
        pbuf = pbuf + LXZ_HEX_OCTET_NUM;
    }

    if (ilen > 0)
    {
        i = 0;
        memset(lhexbuf, 0, sizeof(lhexbuf));
        while (i < ilen)
        {
            sprintf(&(lhexbuf[3 * i]),"%02x ", pbuf[i]);
            i++;
        }
        lxz_log_f_printf("HEX==%s\r\n", lhexbuf);

        pbuf = pbuf + ilen;
        ilen = 0;
    }
    lxz_log_f_printf("================HEX-String-End================\r\n");
    lxz_log_f_printf("\r\n");

    return (pbuf - vbuf);
}