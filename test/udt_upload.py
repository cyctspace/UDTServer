import re
import os
import sys
import time
import socket


udt_svr_addr = "127.0.0.1"
udt_svr_port = 21828

udt_cmd_header = "UT"
udt_cmd_param1 = sys.argv[1]
udt_cmd_param2 = "0"
udt_cmd_full = udt_cmd_header+":"+udt_cmd_param1+":"+udt_cmd_param2

udt_fn_sending = udt_cmd_param1
udt_fp_sending = ""

if __name__ == '__main__':
  udt_cli_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  
  udt_cli_sock.connect((udt_svr_addr, udt_svr_port))
  time.sleep(5)

  print "> " + udt_cmd_full
  udt_cli_sock.send(udt_cmd_full)
  time.sleep(5)

  udt_fp_sending = open(udt_fn_sending, 'rb')  
  udt_data_read = udt_fp_sending.read(1024)
  while udt_data_read:
    udt_cli_sock.send(udt_data_read)
    udt_data_read = udt_fp_sending.read(1024)

  udt_fp_sending.close()
  udt_cli_sock.close()
  print "< OK"
  
