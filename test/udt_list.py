import re
import os
import socket
import time


udt_svr_addr = "127.0.0.1"
udt_svr_port = 21828

udt_cmd_header = "LT"
udt_cmd_param1 = "download"
udt_cmd_param2 = "0"
udt_cmd_full = udt_cmd_header+":"+udt_cmd_param1+":"+udt_cmd_param2

if __name__ == '__main__':
  udt_cli_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  
  udt_cli_sock.connect((udt_svr_addr, udt_svr_port))
  time.sleep(5)

  print "> " + udt_cmd_full
  udt_cli_sock.send(udt_cmd_full)

  udt_data_recved = udt_cli_sock.recv(1024)
  while udt_data_recved:
    print udt_data_recved
    udt_data_recved = udt_cli_sock.recv(1024)

  udt_cli_sock.close()
  print "< OK"
  
