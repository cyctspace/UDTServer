#========================================
# A Simple Mafile for C/C++ Program
# Author: numax
# Date: 2015/06/28
#========================================


#========================================
# Get Current Operating System Name
#========================================
OS_NAME := $(shell uname -s)

RM := rm -rf
ECHO := echo
COPY := cp -f
MKDIR := mkdir
MAKE := make

#========================================
# Current Project Name
#========================================
XDBG_CPN1 := SWRServer
XDBG_CPN2 := UDTServer

#========================================
# All directory for compiling 
#========================================
XDBG_RTDIR := xdbg
XDBG_DIRS := $(XDBG_RTDIR) $(XDBG_RTDIR)/$(XDBG_CPN1) $(XDBG_RTDIR)/$(XDBG_CPN2)

XDBG_MKF1 := $(addsuffix .mk, $(XDBG_CPN1))
XDBG_MKF2 := $(addsuffix .mk, $(XDBG_CPN2))
XDBG_BINS := $(XDBG_CPN1) $(XDBG_CPN2)

all:$(XDBG_DIRS) $(XDBG_BINS)
	@$(ECHO) OK

$(XDBG_DIRS):
	$(MKDIR) $@

$(XDBG_CPN1):
	$(MAKE) -f $(XDBG_MKF1)

$(XDBG_CPN2):
	$(MAKE) -f $(XDBG_MKF2)

.PHONY:all check clean install
check: 
	@$(ECHO) OS_NAME: $(OS_NAME)
	@$(ECHO) CC: $(CC)
	@$(ECHO) CFLAGS: $(CFLAGS)
	@$(ECHO) CPP: $(CPP)
	@$(ECHO) CPPFLAGS: $(CPPFLAGS)
	@$(ECHO) CXX: $(CXX)
	@$(ECHO) CXXFLAGS: $(CXXFLAGS)
	@$(ECHO) CXXPP: $(CXXPP)
	@$(ECHO) CXXPPFLAGS: $(CXXPPFLAGS)
	@$(ECHO) $(shell uname -a)

clean:
	-$(RM) $(XDBG_RTDIR)

install:
	@$(ECHO) !!!Sorry, Unsupported Now!!!

