#include "lxz_config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "lxz_types.h"
#include "lxz_debug.h"
#include "lxz_runlog.h"
#include "lxz_exec.h"
#include "os_port.h"

static uint32 ThreadCycleCheck(void* lpParam);

static uint32 ThreadCycleCheck(void* lpParam)
{
    sint32 i_recv_len = 0;
    sint32 i_echo_times = 0;

    time_t curTime;
    sint32 i_is_timeout = 0;
    sint32 i_file_size = 0;
    lxz_exec_t* p_exec_ctxt = (lxz_exec_t*)lpParam;

    if (p_exec_ctxt != NULL)
    {
        OS_DBG_LOG(("iID:%u, ThreadCycleCheck, start!\r\n", p_exec_ctxt->iThreadID));
        OS_DBG_URC(("iID:%u, ThreadCycleCheck, start!\r\n", p_exec_ctxt->iThreadID));
        while (1)
        {
            if (p_exec_ctxt->iCurStatus == 1)
            {
                i_is_timeout = 0;
                curTime = time(NULL);
                if (curTime - p_exec_ctxt->iStartTime > p_exec_ctxt->iCycleTime)
                {
                    i_is_timeout = 1;
                }

                i_file_size = lxz_log_f_fsize();
                if (   (i_file_size >= LXZ_LOGFILE_MAX_SIZE)
                    || ((1==i_is_timeout) && (i_file_size>=LXZ_LOGFILE_MIN_SIZE)))
                {
                    p_exec_ctxt->iStartTime = curTime;
                    p_exec_ctxt->fp_f_exec(p_exec_ctxt->param1);
                }
            }
            
            osp_timer_f_sleep(1000);
        }
        
        OS_DBG_LOG(("iID:%u, ThreadCycleCheck, stop!\r\n", p_exec_ctxt->iThreadID));
        OS_DBG_URC(("iID:%u, ThreadCycleCheck, stop!\r\n", p_exec_ctxt->iThreadID));
        
        free(p_exec_ctxt);
    }

    return 0;
}

lxz_exec_t* lxz_exec_f_open(fp_exec_t fp_exec_func, void* vp_exec_arg, uint32 u_cycle_time)
{
    lxz_exec_t* p_exec_ctxt = NULL;
    sint32 i_op_status = 0;

    p_exec_ctxt = (lxz_exec_t*)malloc(sizeof(lxz_exec_t));
    if (p_exec_ctxt == NULL)
    {
        return p_exec_ctxt;
    }

    memset(p_exec_ctxt, 0, sizeof(lxz_exec_t));
    p_exec_ctxt->iStartTime = time(NULL);
    p_exec_ctxt->iCycleTime = u_cycle_time;
    p_exec_ctxt->fp_f_exec = fp_exec_func;
    p_exec_ctxt->param1 = vp_exec_arg;
    p_exec_ctxt->iCurStatus = 1;

    i_op_status = osp_thread_f_open(ThreadCycleCheck, p_exec_ctxt, &(p_exec_ctxt->iThreadID));
    if (i_op_status == 0)
    {
        free(p_exec_ctxt);
        p_exec_ctxt = NULL;
    }

    return p_exec_ctxt;
}

sint32 lxz_exec_f_stop(lxz_exec_t* p_exec_ctxt)
{
    sint32 i_ret_val = 0;

    if (p_exec_ctxt != NULL)
    {
        i_ret_val = 1;
        p_exec_ctxt->iCurStatus = 0;
    }

    return 0;
}

sint32 lxz_exec_f_start(lxz_exec_t* p_exec_ctxt, sint32 i_recalc_flag)
{
    sint32 i_ret_val = 0;

    if (p_exec_ctxt != NULL)
    {
        i_ret_val = 1;
        if (i_recalc_flag == 1)
        {
            p_exec_ctxt->iStartTime = time(NULL);
        }
        p_exec_ctxt->iCurStatus = 1;
    }

    return 0;
}

sint32 lxz_exec_f_close(lxz_exec_t* p_exec_ctxt)
{
    sint32 i_ret_val = 0;

    if (p_exec_ctxt != NULL)
    {
        i_ret_val = 1;
        p_exec_ctxt->iCurStatus = 0;

        osp_thread_f_close(&(p_exec_ctxt->iThreadID));
    }

    return 0;
}