#========================================
# A Simple Mafile for C/C++ Program
# Author: numax
# Date: 2016/09/21
#========================================


#========================================
# Get Current Operating System Name
#========================================
OS_NAME := $(shell uname -s)


#========================================
# All essential commands and tools
#========================================
RM := rm -rf
ECHO := echo
COPY := cp -f
MKDIR := mkdir
TOUCH := touch


#========================================
# All information related to version 
#========================================
XDBG_MAJOR := 0
XDBG_MINOR := 0
XDBG_REFIX := 1
XDBG_NOW := $(shell date "+%Y %m %d %H %M %S")
XDBG_YEAR  := $(word 1,$(XDBG_NOW))
XDBG_MONTH  := $(word 2,$(XDBG_NOW))
XDBG_DAY  := $(word 3,$(XDBG_NOW))
XDBG_HOUR  := $(word 4,$(XDBG_NOW))
XDBG_MINUTE  := $(word 5,$(XDBG_NOW))
XDBG_SECOND  := $(word 6,$(XDBG_NOW))
XDBG_BTIME  := $(XDBG_YEAR)$(XDBG_MONTH)$(XDBG_DAY)$(XDBG_HOUR)$(XDBG_MINUTE)$(XDBG_SECOND)

#========================================
# Current Project Name
#========================================
XDBG_CPN := building


#========================================
# All directorys for compiling 
#========================================
XDBG_RTDIR := xdbg
XDBG_MYDIR := $(XDBG_RTDIR)/$(XDBG_CPN)
XDBG_DIRS := $(XDBG_RTDIR) $(XDBG_MYDIR)


#========================================
# All files for compiling 
#========================================
XDBG_HFS := xdbg_version.h

XDBG_HFS := $(addprefix $(XDBG_MYDIR)/, $(XDBG_HFS))


all:$(XDBG_DIRS) new
	@$(ECHO) OK

$(XDBG_DIRS):
	@$(MKDIR) $@

.PHONY:new check clean cleanall install
new:
	-@$(RM) $(XDBG_HFS)
	@$(ECHO) "#ifndef __XDBG_VERSION_H__ " > $(XDBG_HFS)
	@$(ECHO) "#define __XDBG_VERSION_H__ " >> $(XDBG_HFS)
	@$(ECHO) "" >> $(XDBG_HFS)

	@$(ECHO) -n "#define XSW_CUR_MAJOR " >> $(XDBG_HFS)
	@$(ECHO) $(XDBG_MAJOR) >> $(XDBG_HFS)
	@$(ECHO) -n "#define XSW_CUR_MINOR " >> $(XDBG_HFS)
	@$(ECHO) $(XDBG_MINOR) >> $(XDBG_HFS)
	@$(ECHO) -n "#define XSW_CUR_REFIX " >> $(XDBG_HFS)
	@$(ECHO) $(XDBG_REFIX) >> $(XDBG_HFS)

	@$(ECHO) -n '#define XSW_CUR_BTIME ' >> $(XDBG_HFS)
	@$(ECHO) -n '"' >> $(XDBG_HFS)
	@$(ECHO) -n $(XDBG_BTIME) >> $(XDBG_HFS)
	@$(ECHO) '"' >> $(XDBG_HFS)
	@$(ECHO) -n '#define XSW_CUR_YEAR ' >> $(XDBG_HFS)
	@$(ECHO) $(XDBG_YEAR) >> $(XDBG_HFS)
	@$(ECHO) -n '#define XSW_CUR_MONTH ' >> $(XDBG_HFS)
	@$(ECHO) $(XDBG_MONTH) >> $(XDBG_HFS)
	@$(ECHO) -n '#define XSW_CUR_DAY ' >> $(XDBG_HFS)
	@$(ECHO) $(XDBG_DAY) >> $(XDBG_HFS)
	@$(ECHO) -n '#define XSW_CUR_HOUR ' >> $(XDBG_HFS)
	@$(ECHO) $(XDBG_HOUR) >> $(XDBG_HFS)
	@$(ECHO) -n '#define XSW_CUR_MINUTE ' >> $(XDBG_HFS)
	@$(ECHO) $(XDBG_MINUTE) >> $(XDBG_HFS)
	@$(ECHO) -n '#define XSW_CUR_SECOND ' >> $(XDBG_HFS)
	@$(ECHO) $(XDBG_SECOND) >> $(XDBG_HFS)
	@$(ECHO) "" >> $(XDBG_HFS)

	@$(ECHO) "#endif /* __XDBG_VERSION_H__ */ " >> $(XDBG_HFS)
	@$(ECHO) "/* --End-Of-File-- */ " >> $(XDBG_HFS)
	@$(ECHO) "" >> $(XDBG_HFS)
	@$(COPY) $(XDBG_HFS) $(CURDIR)

check: 
	@$(ECHO) OS_NAME: $(OS_NAME)
	@$(ECHO) $(shell uname -a)

clean:
	-$(RM) $(XDBG_MYDIR)
	
cleanall:
	-$(RM) $(XDBG_RTDIR)

install:
	@$(ECHO) !!!Sorry, Unsupported Now!!!

