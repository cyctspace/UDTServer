#include <stdio.h>
#include <string.h>
#include "lxz_types.h"

#define LXZ_HASH_SIZE 32
#define LXZ_CROR(value, bits) ((value >> bits) | (value << (LXZ_HASH_SIZE - bits)))
#define LXZ_CROL(value, bits) ((value << bits) | (value >> (LXZ_HASH_SIZE - bits)))


uint32 lxz_hash_f_ROR(uint32 u_init_value, const uint08 * p_data_buf, sint32 i_data_size)
{
    uint32 u_ret_value = u_init_value;
    uint32 u_tmp_value = 0;
    sint32 i = 0;
    
    while (i < i_data_size) 
    {
        u_tmp_value = (uint32)(0x000000ff & p_data_buf[i]);
        u_ret_value = u_ret_value+u_tmp_value; 
        u_ret_value = LXZ_CROR(u_ret_value, 13);

        i++;
    }

    return u_ret_value;
}

uint32 lxz_hash_f_ROL(uint32 u_init_value, const uint08 * p_data_buf, sint32 i_data_size)
{
    uint32 u_ret_value = u_init_value;
    uint32 u_tmp_value = 0;
    sint32 i = 0;
    
    while (i < i_data_size) 
    {
        u_tmp_value = (uint32)(0x000000ff & p_data_buf[i]);
        u_ret_value = u_ret_value+u_tmp_value; 
        u_ret_value = LXZ_CROL(u_ret_value, 13);

        i++;
    }

    return u_ret_value;
}

#ifdef LXZ_HASH_DBG
int main()
{

    return 0;
}

int lxz_test1()
{
    uint32 u_hash_value = 0;
    uint08* p_tst_str1 = "1234567890";

    u_hash_value = lxz_hash_f_ROR(0x11223344, p_tst_str1, strlen(p_tst_str1));
    printf("lxz, lxz_hash_f_ROR, u_hash_value=0x%08x\r\n", u_hash_value);
    u_hash_value = lxz_hash_f_ROL(0x55667788, p_tst_str1, strlen(p_tst_str1));
    printf("lxz, lxz_hash_f_ROR, u_hash_value=0x%08x\r\n", u_hash_value);

    return 0;
}
#endif /* LXZ_HASH_DBG */
