#ifndef __LXZ_TYPES_H__
#define __LXZ_TYPES_H__

#ifndef uint32
typedef unsigned int uint32;
#endif

#ifndef uint16
typedef unsigned short uint16;
#endif

#ifndef uint08
typedef unsigned char uint08;
#endif

#ifndef sint32
typedef  long sint32;
#endif

#ifndef sint16
typedef  short sint16;
#endif

#ifndef sint08
typedef char sint08;
#endif

#endif /* __LXZ_TYPES_H__ */

