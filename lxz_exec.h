#ifndef __LXZ_EXEC_H__ 
#define __LXZ_EXEC_H__

/** Function prototype for exec functions */
typedef sint32 (*fp_exec_t)(void* arg);

typedef struct _lxz_exec_t
{
    uint32 iThreadID;
    uint32 iCurStatus;
    sint32 iStartTime;
    sint32 iCycleTime;
    fp_exec_t fp_f_exec;
    void* param1;
}lxz_exec_t;

lxz_exec_t* lxz_exec_f_open(fp_exec_t fp_exec_func, void* vp_exec_arg, uint32 u_cycle_time);
sint32 lxz_exec_f_stop(lxz_exec_t* p_exec_ctxt);
sint32 lxz_exec_f_start(lxz_exec_t* p_exec_ctxt, sint32 i_recalc_flag);
sint32 lxz_exec_f_close(lxz_exec_t* p_exec_ctxt);

#endif /* __LXZ_EXEC_H__ */

