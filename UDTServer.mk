#========================================
# A Simple Mafile for C/C++ Program
# Author: numax
# Date: 2015/06/28
#========================================


#========================================
# Get Current Operating System Name
#========================================
OS_NAME := $(shell uname -s)

#========================================
# All essential commands and tools
#========================================
RM := rm -rf
ECHO := echo
COPY := cp -f
MKDIR := mkdir

#========================================
# C Compiler and Options 
#========================================
CC := gcc
CDEF1 := -DSWR_EXTEND
CDEFS := $(CDEF1)
CFLAGS := -O0 -g $(CDEFS)

#========================================
# C Pragram Pre-treatment and Options 
#========================================
CPP := gcc -E
CPPFLAGS :=

#========================================
# C++ Compiler and Compile-Options 
#========================================
CXX := g++
CXXDEF1 := -DSWR_EXTEND
CXXDEFS := $(CXXDEF1)
CXXFLAGS := -O0 -g $(CXXDEFS)

#========================================
# C++ Pragram Pre-treatment and Options 
#========================================
CXXPP := g++ -E
CXXPPFLAGS :=

#========================================
# Link flags and essential third librarys
#========================================
LDFLAGS := -L.
LDLIBS := -lpthread

#========================================
# Current Project Name
#========================================
XDBG_CPN := UDTServer

#========================================
# All directory for compiling 
#========================================
XDBG_RTDIR := xdbg
XDBG_MYDIR := $(XDBG_RTDIR)/$(XDBG_CPN)
XDBG_DIRS := $(XDBG_RTDIR) $(XDBG_MYDIR)


XDBG_SRC := lxz_debug.c \
            lxz_exec.c \
            lxz_runlog.c \
            lxz_string.c \
            main.c \
            os_port.c \
            udtserver.c 

XDBG_OBJS := $(addsuffix .o, $(basename $(XDBG_SRC)))
XDBG_OBJS := $(addprefix $(XDBG_MYDIR)/, $(XDBG_OBJS))

XDBG_BIN := $(addprefix $(XDBG_MYDIR)/, $(XDBG_CPN))

all:$(XDBG_DIRS) $(XDBG_BIN)
	@$(ECHO) OK

$(XDBG_DIRS):
	$(MKDIR) $@

$(XDBG_BIN):$(XDBG_OBJS)
	$(CC) $(CFLAGS) -o $@ $(XDBG_OBJS) $(LDFLAGS) $(LDLIBS)

$(XDBG_MYDIR)/%.o:%.c
	$(CC) $(CFLAGS) -c $< -o $@


.PHONY:check clean cleanall install
check: 
	@$(ECHO) OS_NAME: $(OS_NAME)
	@$(ECHO) CC: $(CC)
	@$(ECHO) CFLAGS: $(CFLAGS)
	@$(ECHO) CPP: $(CPP)
	@$(ECHO) CPPFLAGS: $(CPPFLAGS)
	@$(ECHO) CXX: $(CXX)
	@$(ECHO) CXXFLAGS: $(CXXFLAGS)
	@$(ECHO) CXXPP: $(CXXPP)
	@$(ECHO) CXXPPFLAGS: $(CXXPPFLAGS)
	@$(ECHO) $(shell uname -a)

clean:
	-$(RM) $(XDBG_MYDIR)
	
cleanall:
	-$(RM) $(XDBG_RTDIR)

install:
	@$(ECHO) !!!Sorry, Unsupported Now!!!

