#ifndef __LXZ_STRING_H__
#define __LXZ_STRING_H__

/*
 *  Description:
 *    Get expected string from a specified string.
 *  Param: s, an ASCII string;
 *  Param: o, offsets
 *  Param: c, delimiter
 *  Param: buf, a buffer
 *  Return: p_ret_addr, NULL, fail;NON-NULL, success.
 *  History:
 */
char * lxz_string_f_strtok(const char * s, sint32 o, char c, char * buf);

/*
 *  Description:
 *    Get the first position of a specified character.
 *  Param: s, an ASCII string;
 *  Param: k, an octet character;
 *  Return: i_pos_keychar, -1, fail; >= 0, success.
 *  History:
 */
sint32 lxz_string_f_strkpos(const uint08 * s, char k);

/*
 *  Description:
 *    UpCase all characters that in the specified string.
 *  Param: s, an ASCII string;
 *  Param: k, an octet character;
 *  Return: i_op_status, 0, fail;1, success.
 *  History:
 */
sint32 lxz_string_f_strkupcase(uint08 * s, uint08 k);

/*
 *  Description:
 *    digital string or not.
 *  Param: s, an ASCII string;
 *  Param: l, length of string;
 *  Return: is_digital, 0, false; 1, true.
 *  History:
 */
sint32 lxz_string_f_isdigital(const uint08 * s, sint32 l);

/*
 *  Description:
 *    convert string.
 *  Param: s, an ASCII string;
 *  Param: l, length of string;
 *  Return: .
 *  History:
 */
char * lxz_string_f_strlwr(const char * s, sint32 l);

/*
 *  Description:
 *    convert string.
 *  Param: s, an ASCII string;
 *  Param: l, length of string;
 *  Return: .
 *  History:
 */
char * lxz_string_f_strupr(const char * s, sint32 l);

/*
 *  Description:
 *    delete all specified characters from a string.
 *  Param: s, an ASCII string;
 *  Param: k, an octet character;
 *  Return: .
 *  History:
 */
char* cyc_string_f_delchar(const char * s, char k);

/*
 *  Description:
 *    Converts the hex to bin.
 *  Param: hex_ptr, the hexadecimal format string;
 *  Param: length, the length of hexadecimal string;
 *  Param: bin_ptr, pointer to the binary format string;
 *  Return: FALSE, there's invalid character;
 *  Note: NONE
 */
sint32 lxz_string_f_hex2bin(const uint08 * p_hex_string, sint32 i_hex_length, uint08 * p_bin_buf);

/*
 *  Description:
 *    Converts the bin to hex.
 *  Param: bin_ptr, the binary format string;
 *  Param: length, the length of hexadecimal string;
 *  Param: hex_ptr, pointer to the hexadecimal format string;
 *  Return: 1, success;
 *  Note: NONE
 */
sint32 lxz_string_f_bin2hex(const uint08 * p_bin_string, sint32 i_bin_length, uint08 * p_hex_buf);

#endif /* __LXZ_STRING_H__ */

