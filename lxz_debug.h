#ifndef __LXZ_DEBUG_H__
#define __LXZ_DEBUG_H__

#ifdef OS_PORT_DEBUG
  #define OS_DBG_LOG(arg) do { \
  	lxz_log_f_printf arg;      \
  } while(0)

  #define OS_DBG_URC(arg) do { \
  	lxz_dbg_f_printf arg;      \
  } while(0)

  #define OS_DBG_INF(arg) do { \
  	lxz_dbg_f_printf arg;      \
  } while(0)

  #define OS_DBG_DBG(arg) do { \
  	lxz_dbg_f_printf arg;      \
  } while(0)

  #define OS_DBG_ERR(arg) do { \
  	lxz_dbg_f_printf arg;     \
  } while(0)
#else
  #define OS_DBG_LOG(arg) do { \
  	lxz_log_f_printf arg;      \
  } while(0)

  #define OS_DBG_URC(arg) do { \
  	lxz_dbg_f_printf arg;      \
  } while(0)

  #define OS_DBG_INF(arg) do { \
  } while(0)

  #define OS_DBG_DBG(arg) do { \
  } while(0)

  #define OS_DBG_ERR(arg) do { \
  } while(0)

#endif

sint32 lxz_dbg_f_printf(const char * vfmt, ...);
sint32 lxz_dbg_f_printfhex(const char * vbuf, sint32 ilen);

#endif /* __LXZ_DEBUG_H__ */


