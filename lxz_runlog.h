#ifndef __LXZ_RUNLOG_H__
#define __LXZ_RUNLOG_H__

#define LXZ_DEF_LOG_DIR "runlog"

#define LXZ_LOGFILE_MIN_SIZE ((1024)*(128))
#define LXZ_LOGFILE_MAX_SIZE ((1024)*(512))
#define LXZ_LOGFILE_NEW_CYCLE ((3600)*(24)*(7))


sint32 lxz_log_f_fsize(void);
sint32 lxz_log_f_init(uint08* u_rel_dir);
sint32 lxz_log_f_printf(const char * vfmt, ...);

#endif /* __LXZ_RUNLOG_H__ */

