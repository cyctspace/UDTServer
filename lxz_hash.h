#ifndef __LXZ_HASH_H__
#define __LXZ_HASH_H__

uint32 lxz_hash_f_ROR(uint32 u_init_value, const uint08 * p_data_buf, sint32 i_data_size);
uint32 lxz_hash_f_ROL(uint32 u_init_value, const uint08 * p_data_buf, sint32 i_data_size);


#endif /* __LXZ_HASH_H__ */

