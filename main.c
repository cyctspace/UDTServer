#include "lxz_config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "lxz_types.h"
#include "lxz_debug.h"
#include "lxz_runlog.h"
#include "udtserver.h"

static sint08 gs_root_directory[LXZ_PATH_LENGTH_MAX]={0};

int main(int argc, char *argv[])
{
    int i_ret_val;
    uint16 u_work_port = UDT_SVR_WORK_PORT;

    switch (argc)
    {
    case 2:
        {
            lxz_root_directory_f_set(argv[1]);
            break;
        }
    case 3:
        {
            lxz_root_directory_f_set(argv[1]);
            u_work_port = (uint16)atoi(argv[2]);
            break;
        }
    default:
        {
            break;
        }
    }

    lxz_log_f_init(lxz_root_directory_f_get());
    i_ret_val = udt_server_main(u_work_port);

    return i_ret_val;
}

sint08 * lxz_root_directory_f_get(void)
{
    sint08 * p_root_dir = NULL;

    p_root_dir = (sint08 *)(&gs_root_directory[0]);

    return p_root_dir;
}

sint32 lxz_root_directory_f_set(const sint08 * p_new_rootdirectory)
{
    sint32 i_op_status = 0;

    if (NULL != p_new_rootdirectory)
    {
        memset(gs_root_directory, 0, sizeof(gs_root_directory));
        strcpy(gs_root_directory, p_new_rootdirectory);

        i_op_status = 1;
    }

    return i_op_status;
}

