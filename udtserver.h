#ifndef __UDTESTER_H__
#define __UDTESTER_H__

#define UDT_MAX_CONN_NUM 10
#define UDT_SVR_WORK_PORT 21828


#define UDT_MAX_BUFF_SIZE 16384
#define UDT_DEF_BUFF_SIZE 2048
#define UDT_MIN_BUFF_SIZE 256

#define UDT_CMD_MAX_LEN UDT_MIN_BUFF_SIZE

#define UDT_MAX_BACK_LOG 100

#define LXZ_PATH_LENGTH_MAX 128

typedef struct _udt_context_t
{
    uint32 iThreadID;
    sint32 lsockfd;
    sint32 csockfd;
    uint32 op_type;
    uint32 op_param2;
    sint08 op_param1[UDT_MIN_BUFF_SIZE];
    sint08* p_rw_buff;
}udt_context_t;

int udt_server_main(uint16 u_svr_port);
sint08 * lxz_root_directory_f_get(void);
sint32 lxz_root_directory_f_set(const sint08 * p_new_rootdirectory);

#endif /* __UDTESTER_H__ */

