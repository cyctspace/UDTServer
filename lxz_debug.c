#include "lxz_config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <time.h>

#include "lxz_types.h"
#include "lxz_debug.h"

sint32 lxz_dbg_f_printf(const char * vfmt, ...)
{
    int i = 0;
    int i_str_len = 0;

    char llbuf[257];
    va_list ap;

    va_start(ap,vfmt);
    memset(llbuf, 0x00, sizeof(llbuf));
    i_str_len = vsprintf(llbuf, vfmt, ap);
    va_end(ap);

    while (i<i_str_len)
    {
        putchar(llbuf[i]);
        i++;
    }
    
    return i_str_len;
}

#define LXZ_HEX_OCTET_NUM 16
sint32 lxz_dbg_f_printfhex(const char * vbuf, sint32 ilen)
{
    sint32 i = 0;
    char * pbuf = (char *)vbuf;
    char lhexbuf[4 * LXZ_HEX_OCTET_NUM];

    lxz_dbg_f_printf("\r\n", lhexbuf);
    lxz_dbg_f_printf("================HEX-String-Begin================\r\n");
    while (ilen >= LXZ_HEX_OCTET_NUM)
    {
        i = 0;
        memset(lhexbuf, 0, sizeof(lhexbuf));
        while (i < LXZ_HEX_OCTET_NUM)
        {
            sprintf(&(lhexbuf[3 * i]),"%02x ", pbuf[i]);
            i++;
        }
        lxz_dbg_f_printf("HEX==%s\r\n", lhexbuf);

        ilen = ilen - LXZ_HEX_OCTET_NUM;
        pbuf = pbuf + LXZ_HEX_OCTET_NUM;
    }

    if (ilen > 0)
    {
        i = 0;
        memset(lhexbuf, 0, sizeof(lhexbuf));
        while (i < ilen)
        {
            sprintf(&(lhexbuf[3 * i]),"%02x ", pbuf[i]);
            i++;
        }
        lxz_dbg_f_printf("HEX==%s\r\n", lhexbuf);

        pbuf = pbuf + ilen;
        ilen = 0;
    }
    lxz_dbg_f_printf("================HEX-String-End================\r\n");
    lxz_dbg_f_printf("\r\n");

    return (pbuf - vbuf);
}

